<?php

namespace Drupal\Tests\redirect_metrics\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\redirect\Entity\Redirect;

/**
 * Test the install/uninstall.
 *
 * @group redirect_metrics
 */
class InstallUninstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'link',
    'redirect',
    'path_alias',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('redirect');
  }

  /**
   * Test installing and uninstalling.
   */
  public function testInstallUninstall() {
    // Create a redirect before installing the module to test the initial field
    // values.
    $redirect = Redirect::create([
      'redirect_source' => '/foo',
      'redirect_redirect' => ['uri' => 'internal:/bar'],
    ]);
    $redirect->save();

    $this->container->get('module_installer')->install(['redirect_metrics']);

    // Initial field values should be NULL. Redirects should be considered
    // never accessed until they are actually accessed. The redirect creation
    // date is set from when the metrics module was first installed.
    $redirect = Redirect::load($redirect->id());
    $this->assertEquals(NULL, $redirect->last_access->value);
    $this->assertEquals(0, $redirect->access_count->value);

    // General storage operations should work.
    $redirect->access_count = 2;
    $redirect->save();
    $redirect = Redirect::load($redirect->id());
    $this->assertEquals(2, $redirect->access_count->value);

    // Uninstall should remove fields.
    $this->container->get('module_installer')->uninstall(['redirect_metrics']);
    $redirect = Redirect::load($redirect->id());
    $this->assertFalse($redirect->hasField('access_count'));
    $this->assertFalse($redirect->hasField('last_access'));
  }

}
