<?php

namespace Drupal\Tests\redirect_metrics\Functional;

use Drupal\redirect\Entity\Redirect;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the reports.
 *
 * @group redirect_metrics
 */
class RedirectReportsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'redirect_metrics',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the metrics collection.
   */
  public function testRedirectReports() {
    $this->drupalLogin($this->drupalCreateUser([
      'administer redirects',
      'access content',
    ]));

    $new_redirect = Redirect::create();
    $new_redirect->setSource('foo');
    $new_redirect->setRedirect('node');
    $new_redirect->setStatusCode(301);
    $new_redirect->save();

    $stale_redirect = Redirect::create();
    $stale_redirect->setSource('bar');
    $stale_redirect->setRedirect('node');
    $stale_redirect->setStatusCode(301);
    $stale_redirect->save();
    $stale_redirect->last_access->value = 10000;
    $stale_redirect->access_count->value = 5;
    $stale_redirect->save();

    // The popular report will show all aliases, ordered by the most used one.
    $this->drupalGet('admin/config/search/redirect/popular');
    $this->assertSession()->elementContains('css', 'tbody tr:first-child td:nth-child(2)', 'bar');
    $this->assertSession()->elementContains('css', 'tbody tr:first-child td:nth-child(6)', '5');
    $this->assertSession()->elementContains('css', 'tbody tr:last-child td:nth-child(2)', 'foo');
    $this->assertSession()->elementContains('css', 'tbody tr:last-child td:nth-child(5)', 'Never');
    $this->assertSession()->elementContains('css', 'tbody tr:last-child td:nth-child(6)', '0');

    // Stale redirects report will only show the alias with the old "last
    // access" time.
    $this->drupalGet('admin/config/search/redirect/stale');
    $this->assertSession()->pageTextContains('bar');
    $this->assertSession()->pageTextNotContains('foo');
  }

}
