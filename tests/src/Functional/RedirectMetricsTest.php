<?php

namespace Drupal\Tests\redirect_metrics\Functional;

use Drupal\redirect\Entity\Redirect;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the metrics collection.
 *
 * @group redirect_metrics
 */
class RedirectMetricsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'redirect_metrics',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the metrics collection.
   */
  public function testRedirectMetrics() {
    $redirect = Redirect::create();
    $redirect->setSource('foo');
    $redirect->setRedirect('node');
    $redirect->setStatusCode(301);
    $redirect->save();

    $this->assertEquals(0, $redirect->access_count->value);
    $this->assertEquals(NULL, $redirect->last_access->value);
    sleep(1);

    $this->drupalGet('/foo');
    $this->assertSession()->addressEquals('/node');

    $redirect = Redirect::load($redirect->id());
    $this->assertEquals(1, $redirect->access_count->value);
    $this->assertNotEquals(NULL, $redirect->last_access->value);
  }

}
