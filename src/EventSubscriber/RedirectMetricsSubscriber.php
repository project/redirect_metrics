<?php

namespace Drupal\redirect_metrics\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\redirect\Entity\Redirect;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * An event subscriber for recording redirect metrics.
 */
class RedirectMetricsSubscriber implements EventSubscriberInterface {

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs RedirectMetricsSubscriber.
   */
  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * Record metrics about redirects.
   */
  public function recordRedirectMetrics(ResponseEvent $event) {
    if (!$redirect = $this->resolveRedirect($event)) {
      return;
    }
    // Saving the redirect will invalidate caches related to the redirect. There
    // are three caches we need to evaluate and consider to ensure we aren't
    // introducing performance issues by saving the redirect in the request
    // event:
    // - Dynamic page cache: Since redirects own event subscriber stops
    //   propagation of the request event when a redirect is found, there are
    //   no entries in this cache for any redirects, so invalidating here is
    //   safe.
    // - Page cache: Since this subscriber is a higher priority, the request
    //   workflow will be: stats recorded (tag invalidated), redirect response
    //   generated, page_cache entry created. Since the tags are invalidated
    //   before the cache entry is generated, the responses will still be
    //   cached for all anonymous users and only invalidated when an
    //   authenticated user records a visit or the entity is otherwise saved
    //   again. This seems like an acceptable scenario, given anonymous
    //   traffic will always use a cached version and other reverse proxies
    //   will also receive cacheability headers indicating a response can be
    //   cached.
    // - Other: Other things tagged with the responses, such as the admin
    //   reports of redirects. Since we're actually showing metrics on these
    //   reports invalidating these caches is actually helpful.
    $redirect->access_count->value = $redirect->access_count->value + 1;
    $redirect->last_access->value = $redirect->last_access->value = $this->time->getRequestTime();
    $redirect->save();
  }

  /**
   * Resolve a redirect from the current request.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event.
   *
   * @return \Drupal\redirect\Entity\Redirect|null
   *   The redirect or NULL if one could not be resolved.
   */
  protected function resolveRedirect(ResponseEvent $event) {
    $response = $event->getResponse();
    $redirect_id = $response->headers->get('X_REDIRECT_ID');
    if ($redirect_id && $redirect = Redirect::load($redirect_id)) {
      return $redirect;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => [['recordRedirectMetrics']],
    ];
  }

}
