<?php

namespace Drupal\redirect_metrics;

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * A class for defining fields for metrics.
 */
class RedirectMetricsFieldDefinitions {

  /**
   * Get a list of field definitions to add to the redirect entity.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   *   A list of fields.
   */
  public static function getAll() {
    $fields = [];

    $fields['last_access'] = BaseFieldDefinition::create('timestamp')
      ->setRequired(TRUE)
      ->setTargetEntityTypeId('redirect')
      ->setName('last_access')
      // The module installer automatically installs new fields, so
      // unfortunately the initial field value must be set straight from the
      // hook_entity_base_field_info hook for this to be picked up on install.
      // This means that the definition contains a value that changes with the
      // passage of time. Fortunately however the initial field value is
      // excluded from the properties which are used when computing schema
      // definition mismatches, see
      // SqlContentEntityStorageSchema::processFieldStorageSchema.
      ->setProvider('redirect_metrics')
      ->setDescription(t('The last time the redirect was accessed.'))
      ->setLabel(t('Last accessed'));

    $fields['access_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Access count'))
      ->setTargetEntityTypeId('redirect')
      ->setName('access_count')
      ->setInitialValue(0)
      ->setProvider('redirect_metrics')
      ->setDescription(t('The number of times the redirect has been used.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
